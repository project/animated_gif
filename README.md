# Animated Gif

This module allows you to use animated GIFs on your site.

If the mime-type of the file is image/gif, the image formatter will ignore the
image styles and renders the original GIF.

**Use this with caution!** There are no image styles applied, so the image
dimensions will not be changed and can break your styling!


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.
